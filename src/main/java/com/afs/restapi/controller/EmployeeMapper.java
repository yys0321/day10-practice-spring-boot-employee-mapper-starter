package com.afs.restapi.controller;

import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

//依赖注入 dependency inject
@Component
public class EmployeeMapper {
    //    toEntity employeeRequest => employee entity
    public Employee toEntity(EmployeeRequest request) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(request, employee);
        return employee;
    }

    ///  toResponse employee entity => employeeRequest
    public static EmployeeResponse toResponse(Employee employee) {
        EmployeeResponse employeeResponse = new EmployeeResponse();
        BeanUtils.copyProperties(employee, employeeResponse);
        return employeeResponse;
    }
}